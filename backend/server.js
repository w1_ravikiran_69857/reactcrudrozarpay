

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');
const Razorpay =require('razorpay');

const app = express();
const port = 8888;

app.use(cors());
app.use(bodyParser.json());

const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'ravi',
  database: 'employee',
});

db.connect(err => {
  if (err) {
    throw err;
  }
  console.log('MySQL connected...');
});

app.post('/signup', (req, res) => {
  const { firstName, middleName, lastName, email, username, password, idProof, idProofNumber, address, city, tahshil, district, state, pincode } = req.body;

  console.log('Received signup data:', req.body); 
  const sql = 'INSERT INTO users (firstName, middleName, lastName, email, username, password, idProof, idProofNumber, address, city, tahshil, district, state, pincode) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  const values = [firstName, middleName, lastName, email, username, password, idProof, idProofNumber, address, city, tahshil, district, state, pincode];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error inserting data:', err); 
      return res.status(500).send(err);
    }
    console.log('User registered successfully');
    res.send('User registered successfully');
  });
});

app.post('/login', (req, res) => {
  const sql = "SELECT * FROM users WHERE `email` = ? AND `password` = ?";
  db.query(sql, [req.body.email, req.body.password], (err, data) => {
    if (err) {
      return res.json("Error");
    }
    if (data.length > 0) {
      return res.json("Success");
    } else {
      return res.json("fail");
    }
  });
});

// app.post('/login', (req, res) => {
//   const sql = "SELECT * FROM users WHERE `email` = ? AND `password` = ?";
//   db.query(sql, [req.body.email, req.body.password], (err, data) => {
//     if (err) {
//       return res.json({ success: false, error: "Error" });
//     }
//     if (data.length > 0) {
//       return res.json({ success: true, message: "Success", email: req.body.email });
//     } else {
//       return res.json({ success: false, message: "Invalid email or password" });
//     }
//   });
// });


app.get('/users', (req, res) => {
  const sql = "SELECT * FROM users";
  db.query(sql, (err, data) => {
    if (err) {
      console.error("Error retrieving data:", err);
      return res.json({ success: false, error: err });
    }
    return res.json({ success: true, users: data });
  });
});

app.get('/users/:id', (req, res) => {
  const { id } = req.params;
  const sql = "SELECT * FROM users WHERE id = ?";
  db.query(sql, [id], (err, data) => {
    if (err) {
      console.error("Error retrieving user data:", err);
      return res.json({ success: false, error: err });
    }
    if (data.length > 0) {
      return res.json({ success: true, user: data[0] });
    } else {
      return res.json({ success: false, message: "User not found" });
    }
  });
});

app.put('/users/:id', (req, res) => {
  const { id } = req.params;
  const { firstName, middleName, lastName, username, idProof, idProofNumber, address, city, tahshil, district, state, pincode } = req.body;
  const sql = "UPDATE users SET firstName = ?, middleName = ?, lastName = ?, username = ?, idProof = ?, idProofNumber = ?, address = ?, city = ?, tahshil = ?, district = ?, state = ?, pincode = ? WHERE id = ?";
  const values = [firstName, middleName, lastName, username, idProof, idProofNumber, address, city, tahshil, district, state, pincode, id];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error("Error updating data:", err);
      return res.json({ success: false, error: err });
    }
    return res.json({ success: true, message: 'User updated successfully' });
  });
});

// Razorpay payment gateway Integration
app.post('/orders', async (req, res) => {
  const razorpay = new Razorpay({
    key_id: "rzp_test_R6HMBFB20bnBeV",
    key_secret: "DxwngGRfoWmk5gW6lMKz8yeh"
  });
  const options = {
    amount: req.body.amount,
    currency: req.body.currency,
    receipt: "receipt#1",
    payment_capture: 1
  };
  try {
    const response = await razorpay.orders.create(options);
    res.json({
      order_id: response.id,
      currency: response.currency,
      amount: response.amount
    });
  } catch (error) {
    res.status(500).send("internal server error");
  }
});

app.get("/payment/:paymentid", async (req, res) => {
  const { paymentid } = req.params;
  const razorpay = new Razorpay({
    key_id: "rzp_test_R6HMBFB20bnBeV",
    key_secret: "DxwngGRfoWmk5gW6lMKz8yeh"
  });
  try {
    const payment = await razorpay.payments.fetch(paymentid);
    if (!payment) {
      return res.status(500).json('error at razorpay loading');
    }
    res.json({
      status: payment.status,
      method: payment.method,
      amount: payment.amount,
      currency: payment.currency
    });
  } catch (error) {
    res.status(500).json("failed to fetch");
  }
});
//


app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});





