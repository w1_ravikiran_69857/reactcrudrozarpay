import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import './editprofile.css';

const EditProfile = () => {
    const [user, setUser] = useState(null); 
    const navigate = useNavigate();
    const { id } = useParams();

    useEffect(() => {
        axios.get(`http://localhost:8888/users/${id}`)
            .then(res => {
                if (res.data.success) {
                    setUser(res.data.user);
                } else {
                    console.error("Error fetching user:", res.data.error);
                }
            })
            .catch(err => {
                console.error("Error fetching user:", err);
            });
    }, [id]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    };

    const handleUpdate = (e) => {
        e.preventDefault();
        axios.put(`http://localhost:8888/users/${id}`, user)
            .then(res => {
                if (res.data.success) {
                    navigate('/home');
                } else {
                    console.error("Error updating user:", res.data.error);
                }
            })
            .catch(err => {
                console.error("Error updating user:", err);
            });
    };

    if (!user) {
        return <div>Loading...</div>;
    }

    return (
        <div className="edit-form-container">
            <h2>Edit Profile</h2>
            <form onSubmit={handleUpdate} className="edit-form">
                <table>
                    <tbody>
                        <tr>
                            <td><label>First Name</label></td>
                            <td><input type="text" name="firstName" value={user.firstName} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Middle Name</label></td>
                            <td><input type="text" name="middleName" value={user.middleName} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Last Name</label></td>
                            <td><input type="text" name="lastName" value={user.lastName} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Username</label></td>
                            <td><input type="text" name="username" value={user.username} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>ID Proof</label></td>
                            <td><input type="text" name="idProof" value={user.idProof} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>ID Proof Number</label></td>
                            <td><input type="text" name="idProofNumber" value={user.idProofNumber} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Address</label></td>
                            <td><input type="text" name="address" value={user.address} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>City</label></td>
                            <td><input type="text" name="city" value={user.city} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Tahshil</label></td>
                            <td><input type="text" name="tahshil" value={user.tahshil} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>District</label></td>
                            <td><input type="text" name="district" value={user.district} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>State</label></td>
                            <td><input type="text" name="state" value={user.state} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td><label>Pincode</label></td>
                            <td><input type="text" name="pincode" value={user.pincode} onChange={handleInputChange} /></td>
                        </tr>
                        <tr>
                            <td colSpan="2" style={{ textAlign: 'center' }}>
                                <button type="submit">Update</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    );
};

export default EditProfile;
