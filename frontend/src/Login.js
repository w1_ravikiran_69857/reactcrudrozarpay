import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

const Login = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const navigate = useNavigate();

  const [errors, setErrors] = useState({
    email: '',
    password: '',
  });

  const handleInput = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const validate = (values) => {
    const errors = {};
    if (!values.email) {
      errors.email = 'Email is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
      errors.email = 'Email address is invalid';
    }

    if (!values.password) {
      errors.password = 'Password is required';
    } else if (values.password.length < 6) {
      errors.password = 'Password must be at least 6 characters';
    }

    return errors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const validationErrors = validate(formData);
    console.log('Form data:', formData);
    setErrors(validationErrors);

    if (Object.keys(validationErrors).length === 0) {
      axios.post("http://localhost:8888/login", formData)
        .then(res => {
          if (res.data === "Success") {
            if (formData.email === 'gondravikiran@gmail.com' && formData.password === '123456789') {
              navigate('/home');
            } else {
              navigate('/profile');
            }
          } else {
            alert("Invalid email or password");
          }
        })
        .catch(err => console.log(err));
    }
  };

  return (
    <div className='d-flex justify-content-center align-items-center bg-secondary vh-100'>
      <div className='bg-white p-3 rounded w-25'>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor='email'><strong>Email</strong></label>
            <input
              type='email'
              placeholder='Enter Email'
              className='form-control rounded-0'
              name='email'
              value={formData.email}
              onChange={handleInput}
              required
            />
            {errors.email && <p className="text-danger">{errors.email}</p>}
          </div>
          <div className="mb-3">
            <label htmlFor='password'><strong>Password</strong></label>
            <input
              type='password'
              placeholder='Enter password'
              className='form-control rounded-0'
              name='password'
              value={formData.password}
              onChange={handleInput}
              required
            />
            {errors.password && <p className="text-danger">{errors.password}</p>}
          </div>
          <button className='btn btn-success w-100' type='submit'>Login</button>
          <Link to='/signup' className='btn btn-default border w-100 text-decoration-none mt-2'>Create Account</Link>
        </form>
      </div>
    </div>
  );
};

export default Login;

