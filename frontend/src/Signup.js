import React, { useState } from 'react';
import Login from './Login';
import './signup.css';


const Signup = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    middleName: '',
    lastName: '',
    email: '',
    username: '',
    password: '',
    idProof: '',
    idProofNumber: '',
    address: '',
    city: '',
    tahshil: '',
    district: '',
    state: '',
    pincode: '',
  });

  const [errors, setErrors] = useState({});

  const validateField = (name, value) => {
    const newErrors = { ...errors };

    switch (name) {
      case 'firstName':
        if (/\d/.test(value)) newErrors.firstName = 'First name cannot contain numbers';
        else delete newErrors.firstName;
        break;
      case 'middleName':
        if (/\d/.test(value)) newErrors.middleName = 'Middle name cannot contain numbers';
        else delete newErrors.middleName;
        break;
      case 'lastName':
        if (/\d/.test(value)) newErrors.lastName = 'Last name cannot contain numbers';
        else delete newErrors.lastName;
        break;
      case 'idProofNumber':
        if (value.length < 10) newErrors.idProofNumber = `${formData.idProof} number must be at least 10 characters long`;
        else delete newErrors.idProofNumber;
        break;
      case 'city':
        if (/\d/.test(value)) newErrors.city = 'City cannot contain numbers';
        else delete newErrors.city;
        break;
      case 'tahshil':
        if (/\d/.test(value)) newErrors.tahshil = 'Tahshil cannot contain numbers';
        else delete newErrors.tahshil;
        break;
      case 'district':
        if (/\d/.test(value)) newErrors.district = 'District cannot contain numbers';
        else delete newErrors.district;
        break;
      case 'state':
        if (/\d/.test(value)) newErrors.state = 'State cannot contain numbers';
        else delete newErrors.state;
        break;
      case 'pincode':
        if (!/^\d{6}$/.test(value)) newErrors.pincode = 'Pincode must be exactly 6 digits';
        else delete newErrors.pincode;
        break;
      default:
        break;
    }

    setErrors(newErrors);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
    validateField(name, value);
  };

  const handleBlur = (e) => {
    const { name, value } = e.target;
    validateField(name, value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const validationErrors = validateAllFields();
    if (Object.keys(validationErrors).length === 0) {
      try {
        const response = await fetch('http://localhost:8888/signup', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });
        const result = await response.text();
        console.log(result);
      } catch (error) {
        console.error('Error:', error);
      }
    } else {
      setErrors(validationErrors);
    }
  };

  const validateAllFields = () => {
    const validationErrors = {};
    Object.keys(formData).forEach((field) => {
      validateField(field, formData[field]);
    });
    return validationErrors;
  };

  return (
    <div className="signup-container">
      <h2>Signup</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="firstName">First Name:</label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            value={formData.firstName}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.firstName && <p className="error">{errors.firstName}</p>}
        </div>
        <div>
          <label htmlFor="middleName">Middle Name:</label>
          <input
            type="text"
            id="middleName"
            name="middleName"
            value={formData.middleName}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {errors.middleName && <p className="error">{errors.middleName}</p>}
        </div>
        <div>
          <label htmlFor="lastName">Last Name:</label>
          <input
            type="text"
            id="lastName"
            name="lastName"
            value={formData.lastName}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.lastName && <p className="error">{errors.lastName}</p>}
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
        </div>
        <div>
          <label htmlFor="username">Username:</label>
          <input
            type="text"
            id="username"
            name="username"
            value={formData.username}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            name="password"
            value={formData.password}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
        </div>
        <div>
          <label htmlFor="idProof">ID Proof:</label>
          <select
            id="idProof"
            name="idProof"
            value={formData.idProof}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          >
            <option value="">Select</option>
            <option value="Aadhar">Aadhar</option>
            <option value="Pan Card">Pan Card</option>
          </select>
        </div>
        {formData.idProof && (
          <div>
            <label htmlFor="idProofNumber">{formData.idProof} Number:</label>
            <input
              type="text"
              id="idProofNumber"
              name="idProofNumber"
              value={formData.idProofNumber}
              onChange={handleChange}
              onBlur={handleBlur}
              required
            />
            {errors.idProofNumber && <p className="error">{errors.idProofNumber}</p>}
          </div>
        )}
        <div>
          <label htmlFor="address">Address:</label>
          <input
            type="text"
            id="address"
            name="address"
            value={formData.address}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
        </div>
        <div>
          <label htmlFor="city">City:</label>
          <input
            type="text"
            id="city"
            name="city"
            value={formData.city}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.city && <p className="error">{errors.city}</p>}
        </div>
        <div>
          <label htmlFor="tahshil">Tahshil:</label>
          <input
            type="text"
            id="tahshil"
            name="tahshil"
            value={formData.tahshil}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.tahshil && <p className="error">{errors.tahshil}</p>}
        </div>
        <div>
          <label htmlFor="district">District:</label>
          <input
            type="text"
            id="district"
            name="district"
            value={formData.district}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.district && <p className="error">{errors.district}</p>}
        </div>
        <div>
          <label htmlFor="state">State:</label>
          <input
            type="text"
            id="state"
            name="state"
            value={formData.state}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.state && <p className="error">{errors.state}</p>}
        </div>
        <div>
          <label htmlFor="pincode">Pincode:</label>
          <input
            type="text"
            id="pincode"
            name="pincode"
            value={formData.pincode}
            onChange={handleChange}
            onBlur={handleBlur}
            required
          />
          {errors.pincode && <p className="error">{errors.pincode}</p>}
        </div>
        <button type="submit" >Signup</button>
      </form>
    </div>
  );
};

export default Signup;



