import axios from 'axios';
import React, { useState } from 'react';

const Profile = () => {
  const [responseId, setResponseId] = useState('');
  const [responseState, setResponseState] = useState('');

  const loadScript = (src) => {
    return new Promise((resolve) => {
      const script = document.createElement('script');
      script.src = src;
      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };
      document.body.appendChild(script);
    });
  };

  const createRazorPayOrder = (amount) => {
    let data = JSON.stringify({
      amount: amount * 100,
      currency: 'INR'
    });
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'http://localhost:8888/orders', 
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    axios.request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        handleRazorpayScreen(response.data.amount);
      })
      .catch((error) => {
        console.log('error at', error);
      });
  };

  const handleRazorpayScreen = async (amount) => {
    const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js');

    if (!res) {
      alert('Some error at razorpayScreen loading');
      return;
    }

    const options = {
      key: 'rzp_test_R6HMBFB20bnBeV',
      amount: amount,
      currency: 'INR',
      name: 'Ravikirans cafe',
      description: 'payment to Ravikirans cafe',
      handler: function (response) {
        setResponseId(response.razorpay_payment_id);
      },
      profile: {
        name: 'Ravikirans cafe',
        email: 'gondravikiran@gmail.com'
      },
      theme: {
        color: '#F4C430'
      }
    };
    const paymentObject = new window.Razorpay(options);
    paymentObject.open();
  };

  const paymentFetch = (e) => {
    e.preventDefault();

    const paymentId = e.target.paymentId.value;
    console.log("paymentid------------",paymentId);

    axios.get(`http://localhost:8888/payment/${paymentId}`) 
      .then((response) => {
        console.log(response.data);
        setResponseState(response.data);
      })
      .catch((error) => {
        console.log('error occurs', error);
      });
  };

  return (
    <div className='App' >
      <div style={{justifyContent:"center", marginLeft:"30px", marginTop:"200px"}}>
      <button onClick={() => createRazorPayOrder(100)}>payment of 100Rs.</button>
      {responseId && <p>{responseId}</p>}
      <h1>this is payment verification form </h1>
      <form onSubmit={paymentFetch}>
        <input type='text' name='paymentId' />
        <button type='submit'>fetch Payment</button>
        {responseState.length !== 0 && (
          <ul>
            <li>Amount: {responseState.amount}</li>
            <li>Currency: {responseState.currency}</li>
            <li>Status: {responseState.status}</li>
            <li>Method: {responseState.method}</li>
            
          </ul>
        )}
      </form>
      </div>
    </div>
  );
};

export default Profile;
